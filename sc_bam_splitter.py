#!/bin/env python
""" Given a single cell cellranger generated BAM file, split it into BAM files, one for each cell according to the CB (cell barcode) tag in the input BAM.


    Usage:
        sc_bam_splitter -i <data_file> -c <cell_barcodes> [-x <out_prefix>] [-o <out_dir>] [-p <n_processes>] [-s <n_reads>]
        sc_bam_splitter -h | --help
        sc_bam_splitter --version

    Options:
        -h --help                           Show this screen.
        --version                           Show version.

        -i --in <bam_file>                  Input BAM file
        -c --cell_barcodes <cell_barcodes>  Fiule with cell barcodes to keep, 1 barcode per line.
        -x --out_prefix <out_prefix>        [default: out]
        -o --out_dir <out_dir>              (default: split_<bam_file>)
        -p --processes <n_processes>        How many processes should be spawned? [default: 1]
        -s --sample <n_reads>               Make a test sample with n_reads reads from each chromosome and run the script on it.
                                            [default: -1]

    Output: the following files are written
        ...
"""



# Ideas
#
# use haplotypecaller on big BAM file
# filter SNPs n pick candidates
# subsample BAM for reads overlapping candidate positions
# split into cell bams for those cells that overlap candidates

# Idea 2:
# encode each cell as a separate RG:ID and sample in a joint BAM file


### other modules
#import regex
#import re
import docopt
import sys
import os
import time
from collections import defaultdict
#import pdb  # Py debugger

import pysam  # for human genome FASTA indexing and for working with BAM files
#from pybedtools import BedTool



# parallel execution
# import multiprocessing as mp
from multiprocessing import Pool


import csv



###
### Global "constants", i.e., do not modify them anywhere.
DEBUG = False

__version__ = 'v1.0.0'






def startupMessage():
    print("\n### pipeline " + __version__ + ' ###\n')


def printTime(msg):
    print( "{}: {}\n".format(msg,time.strftime("%H:%M:%S")) )



def log(file, text, append = True):
    msg = "{}: {}\n".format(text,time.strftime("%H:%M:%S"))

    with open(file, 'a' if append else 'w') as logFile:
        logFile.write(msg)

    print( msg )


def log_run_params(log_file, log_dic):
    for k,v in log_dic.items():
        log(log_file, k + " : " + str(v) + "\n")


def processCommandLineParams():
    ### get arguments
    arguments = docopt.docopt(__doc__, version=__version__)
    if DEBUG: print( arguments )
    #exit(0)

    in_bam = arguments['--in']
    cell_barcodes = arguments['--cell_barcodes']
    out_prefix = arguments['--out_prefix']
    out_dir = arguments['--out_dir']
    n_processes = int(arguments['--processes'])
    n_reads = int(arguments['--sample'])

    
    ### process arguments
    if not out_prefix:
        out_prefix = 'out_'

    (base_name, ext) = os.path.splitext( os.path.basename(in_bam) )  # remove outer .bam, .fastq or .gz extension
    if not out_dir:
        out_dir = 'split_' + base_name
    if not os.path.exists( out_dir ):
        os.makedirs( out_dir )
    if not os.path.exists(cell_barcodes):
        print("{} doesn't exist.".format(cell_barcodes))
        sys.exit(2)


    return (in_bam, cell_barcodes, out_prefix, out_dir, n_processes, n_reads)



def create_sample_file(in_bam, n_reads):
    """
    """

    bam_in = pysam.AlignmentFile(in_bam, "rb")
    bam_sample_name = "bam_sample_" + n_reads + ".bam"
    bam_sample = pysam.AlignmentFile(
                bam_sample_name,
                "wb",
                template = bam_in)

    for i in range(3):  # only use 1st 3 chromosomes in the list
        chrom = bam_in.get_reference_name(i)

        for read in bam_in.fetch(chrom):
            bam_sample.write(read)
    
    bam_sample.close()

    # sort and index
    header = get_header(bams[0])
    bam_out = os.path.join(merged_output_dir, os.path.basename(sample_id)) + '.bam'
    out = pysam.AlignmentFile(bam_out, 'wb', header=header)
    for bam in bams:
        sam = pysam.AlignmentFile(bam, 'rb')
        for read in sam:
            out.write(read)
    out.close()
    pysam.sort(bam_out, 'tmp')
    os.rename('tmp.bam',bam_out)
    pysam.index(bam_out) 
    
    return bam_sample_name



def split_into_RGs(in_bam, cell_barcodes_csv, out_dir):
    """
        Go throug input BAM
            generate header
            write to bam_out
        reheader bam_out with generated header containing RG ID for each cell
        TBD sort n index... or let user do it w samtools

        pre-filter BAM file to only include cells w >= 10k reads => ~9k cells instead of 1.8 mill
    """
    print("A")
    bam_in = pysam.AlignmentFile(in_bam, "rb")
    (base_name, ext) = os.path.splitext( os.path.basename(in_bam) )
    bam_out_name = os.path.join(out_dir, base_name + "_cellsInRG" + ".bam")
    bam_out = pysam.AlignmentFile(bam_out_name, "wb", template = bam_in)

    barcode_set = set()

    cell_barcodes = set()
    with open(cell_barcodes_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        # header = next(csv_reader) #skip header
        for row in csv_reader:
            cell_barcodes.add(row[0])


    print("B")

    #
    # create header
    #
    header = bam_in.header.as_dict()
    header['RG'] = []  # clear RGs

    for bc in cell_barcodes:
        header_RG = {
            'ID': bc,
            'LB': '0.1',
            'PL': 'ILLUMINA',
            'PU': bc,
            'SM': bc
        }
        header['RG'].append(header_RG)

    for read in bam_in:
        # TBD print out the chr that's being processed + number reads processed for that chromosome

        # get cell barcode
        CB_list = [x[1] for x in read.tags if x[0] == "CB"]
        
        if CB_list:
            barcode = CB_list[0][:-2]

            if barcode not in cell_barcodes:
                continue
        else:
            continue   # TBD or use CR???


        # Make new read line containing RG and write to out BAM
        a = pysam.AlignedSegment()
        a.query_name = read.query_name
        a.query_sequence = read.query_sequence
        a.flag = read.flag
        a.reference_id = read.reference_id
        a.reference_start = read.reference_start
        a.mapping_quality = read.mapping_quality
        a.cigar = read.cigar
        a.next_reference_id = read.next_reference_id
        a.next_reference_start = read.next_reference_start
        a.template_length = read.template_length
        a.query_qualities = read.query_qualities

        new_read_tags = [tag for tag in read.tags][:-1]
        new_read_tags.append(('RG', barcode))  # hopefully it's always the last entry TBD
        a.tags = new_read_tags
        bam_out.write(a)


    print("C")

    # make SAM with header only
    sam_out_name = os.path.join(out_dir, base_name + "_cellsInRG_header" + ".sam")
    sam_out_header = pysam.AlignmentFile(sam_out_name, "w", header = header)
    sam_out_header.close()

    print("D")

    # reheader
    #cmd = "samtools reheader {in_header_sam} {out_bam}".format(in_header_sam = sam_out_name, out_bam = bam_out_name)
    #os.system(cmd)

    print("E")




def main():
    startupMessage()

    ## Process command line parameters
    (in_bam, cell_barcodes, out_prefix, out_dir, n_processes, n_reads) = processCommandLineParams()

    ## Initiate log file and write command line parameters.
    # TODO: perhaps change time format so it can b sorted more easily
    global log_file
    log_file = os.path.join('log_' + time.asctime().replace(' ', '_').replace(':', '-') + '.log')
    log(log_file, "Analysis date: " + time.asctime(), append = False)
    log_params = {
        'in_bam' : in_bam,
        'cell_barcodes' : cell_barcodes,
        'out_prefix' : out_prefix,
        'out_dir' : out_dir,
        'n_processes' : n_processes,
        'n_reads': n_reads
    }
    log_run_params(log_file, log_params)


    split_into_RGs(in_bam, cell_barcodes, out_dir)
    sys.exit(2)

    #if n_reads > -1:
    #    in_bam = create_sample_file(bam_in, n_reads)
    
    bam_in = pysam.AlignmentFile(in_bam, "rb")
    chromosomes = bam_in.references[1:25] # [bam_in.get_reference_name(i) for i in range(25)]  # use only 1-22, X, Y, MT

    for chrom in chromosomes:
        # distributes 2 as many processors as there are: multiprocessing.cpu_count()  gives the max number
        # if we want fewer, we can specify tt explicitly
        start_time = time.time()

        pool = Pool(processes = n_processes)
        result = pool.map(split_into_RGs, chromosomes)

        # not optimal but safe TBD TBD y n what does this do?
        pool.close()
        pool.join()
        
        end_time = time.time() - start_time
        print(f"MP took: {end_time}")

    merge_cell_chromosomes()


    bam_in.close()

    log(log_file, "End time: " + time.asctime())



if __name__ == '__main__':
    #print(dir())
    main()



#
# Misc
#
# count number reads per file:
#   for f in split_sc_bam_test/*.bam ; do echo "$f: $(samtools view -c $f)" >> counts.txt ; done